package com.capgemini.corsospringboot.starter;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class StarterApplication implements CommandLineRunner {
	
	@Value("${employee.name}")
	String name;
	
	@Value("${employee.sur}")
	String surname;

	public static void main(String[] args) {
		SpringApplication.run(StarterApplication.class, args);
	}

	@Override
	public void run(String... args) throws Exception {
		List<Employee> lista= new ArrayList<Employee>();
		
		Employee uno= new Employee();
		uno.setName("Stanislao");
		uno.setSurname("Vigna");
		
		lista.add(uno);
		
		Employee due= new Employee();
		due.setName("Marco");
		due.setSurname("Piermartini");
		
		lista.add(due);
		
		Employee tre= new Employee();
		tre.setName(name);
		tre.setSurname(surname);
		
		lista.add(tre);
		
		for (Iterator<Employee> iterator = lista.iterator(); iterator.hasNext();) {
			Employee employee = (Employee) iterator.next();
			
			System.out.println(employee);
		}
		
	}
}