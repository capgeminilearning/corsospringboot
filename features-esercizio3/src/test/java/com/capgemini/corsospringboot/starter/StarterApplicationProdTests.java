package com.capgemini.corsospringboot.starter;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;

@SpringBootTest
@ActiveProfiles("prod")
class StarterApplicationProdTests {

	@Autowired
	private DataBaseConnector connector;

	@Test
	public void test() {
		
		Assertions.assertEquals("prod", connector.connect());
		
	
	}

}
