package com.capgemini.corsospringboot.starter;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class StarterApplication implements CommandLineRunner {
	
	
	//Creare le implementazioni dell'interfaccia connector per fare in modo che i due test girino

	@Autowired
	private DataBaseConnector connector;

	public static void main(String[] args) {
		SpringApplication.run(StarterApplication.class, args);
	}

	@Override
	public void run(String... args) throws Exception {
		System.out.println(connector.connect());
	}
}