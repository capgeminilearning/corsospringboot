package com.capgemini.corsospringboot.starter;

public interface DataBaseConnector {
	
	String connect();

}
