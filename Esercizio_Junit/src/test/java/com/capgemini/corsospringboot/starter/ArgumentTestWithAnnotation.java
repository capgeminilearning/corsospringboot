package com.capgemini.corsospringboot.starter;

import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.ConfigFileApplicationContextInitializer;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;

import com.capgemini.corsospringboot.junit.JunitApplication;
import com.capgemini.corsospringboot.junit.controller.MyProcessor;

@RunWith(SpringRunner.class)
@SpringBootTest
@ContextConfiguration(classes = { JunitApplication.class }, initializers = ConfigFileApplicationContextInitializer.class)
public class ArgumentTestWithAnnotation {

	@Autowired
	private MyProcessor processor;

	@Test
	public void testArgs() {
		String processResult = processor.process();
		System.out.println("process returned value: " + processResult);
		Assertions.assertThat(processResult).isEqualTo("Successfully executed process with name TestProcess");
	}
}
