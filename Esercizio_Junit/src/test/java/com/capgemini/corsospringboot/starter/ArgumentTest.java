package com.capgemini.corsospringboot.starter;

import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.ConfigFileApplicationContextInitializer;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;

import com.capgemini.corsospringboot.junit.JunitApplication;

@RunWith(SpringRunner.class)
@SpringBootTest
@ContextConfiguration(classes = {JunitApplication.class }, initializers = ConfigFileApplicationContextInitializer.class)
public class ArgumentTest {

	@Value("${processor.name}")
	private String processName;

	@Test
	public void testArgs() {
		System.out.println("processor.name: " + processName);
		Assertions.assertThat(processName).isEqualTo("TestProcess");
	}
}
