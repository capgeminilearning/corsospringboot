package com.capgemini.corsospringboot.junit.controller;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;

@Controller
public class MyProcessor {
	@Value("${processor.name}")
	private String processName;

	public String process() {
		return "Successfully executed process with name " + processName;
	}
}
