package com.capgemini.corsospringboot.junit;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class JunitApplication {
	
	
	/*
	 * 
	 * Create un'appicazione Spring Boot che esegua la seguente operazione: A*B+10/C
	 * 
	 * A >10
	 * B <12
	 * 3<C<14
	 * 
	 * Eseguire i JunitTest
	 */
	

	public static void main(String[] args) {
		SpringApplication.run(JunitApplication.class, args);
	}

}
