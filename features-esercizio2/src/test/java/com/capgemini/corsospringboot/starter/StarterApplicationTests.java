package com.capgemini.corsospringboot.starter;


import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
class StarterApplicationTests {
	
	@Autowired
	private MailService mailService;

	@Test
	public void test() {
		assertEquals("corsospring@capgemini.com", mailService.getFrom());
		assertEquals("localhost", mailService.getHostName());
		assertEquals(8080, mailService.getPort());
	
	}

}
