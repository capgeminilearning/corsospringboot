package com.capgemini.corsospringboot.starter;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class BaseConfig {
	
	

	@Bean
	public MailService mailservice(@Value("${mail.hostname}") String hostName, @Value("${mail.port}") int port,
			@Value("${mail.from}") String from) {

		MailService mailService = new MailService();
		mailService.setFrom(from);
		mailService.setHostName(hostName);
		mailService.setPort(port);
		return mailService;

	}

	

}
