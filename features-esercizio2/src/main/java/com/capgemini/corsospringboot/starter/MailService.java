package com.capgemini.corsospringboot.starter;

public class MailService {

	private String hostName;
	private int port;
	private String from;

	public String getHostName() {
		return hostName;
	}

	public void setHostName(String hostName) {
		this.hostName = hostName;
	}

	public int getPort() {
		return port;
	}

	public void setPort(int port) {
		this.port = port;
	}

	public String getFrom() {
		return from;
	}

	public void setFrom(String from) {
		this.from = from;
	}

	public void send(String msg, String to) {

		String mail = String.format("%s ha spedito \"%s\" a %s da %s:%s", from, msg, to, hostName, port);
		System.out.print(mail);
	}

}