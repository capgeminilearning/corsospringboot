package com.capgemini.corsospringboot.starter;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Component
public class Hello {

	@Value("${hello.msg}")
	private String msg;
	

	
	public String getMsg() {
		return msg;
	}
	
	

}
