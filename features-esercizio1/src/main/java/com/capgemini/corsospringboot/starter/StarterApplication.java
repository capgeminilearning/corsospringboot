package com.capgemini.corsospringboot.starter;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class StarterApplication implements CommandLineRunner {
	
	//Fare in modo che la batteria di test giri, senza modificare i metodi @Test e utilizzando tecniche diverse per ogni test

	@Autowired
	private Hello hello;

	public static void main(String[] args) {
		SpringApplication.run(StarterApplication.class, args);
	}

	@Override
	public void run(String... args) throws Exception {
		System.out.println(hello.getMsg());

	}
}