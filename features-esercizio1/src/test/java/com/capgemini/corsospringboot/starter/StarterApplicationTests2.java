package com.capgemini.corsospringboot.starter;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
class StarterApplicationTests2 {

	@Autowired
	private Hello hello;

	@Test
	public void test() {
		assertEquals("Buongiorno", hello.getMsg());
	}

}
